unit DCSSimpleQueueBasedListener;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  DCSHandler,
  dcsListenerThread;

type

  { TDCSSimpleQueueBasedListener }

  TDCSSimpleQueueBasedListener = class(TObject, IDCSListener)
  private
    FEventQueue: TDCSEventQueue;
    FOrigin: string;
    FListenerId: Integer;
  public
    constructor Create(AnOrigin: string; AnEventQueue: TDCSEventQueue);

    function GetListenerId: Integer;
    procedure InitListener(AListenerId: Integer);
    procedure SendEvent(AnEvent: TDCSEvent);
    function GetOrigin: string;
    property ListenerId: Integer read GetListenerId;
  end;

implementation

constructor TDCSSimpleQueueBasedListener.Create(AnOrigin: string; AnEventQueue: TDCSEventQueue);
begin
  FOrigin := AnOrigin;
  FEventQueue := AnEventQueue;
end;

function TDCSSimpleQueueBasedListener.GetListenerId: Integer;
begin
  Result := FListenerId;
end;

procedure TDCSSimpleQueueBasedListener.InitListener(AListenerId: Integer);
begin
  FListenerId := AListenerId;
end;

procedure TDCSSimpleQueueBasedListener.SendEvent(AnEvent: TDCSEvent);
begin
  AnEvent.AddRef;
  FEventQueue.PushItem(AnEvent);
end;

function TDCSSimpleQueueBasedListener.GetOrigin: string;
begin
  Result := FOrigin;
end;

end.

