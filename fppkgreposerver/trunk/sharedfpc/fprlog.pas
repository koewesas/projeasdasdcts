unit fprLog;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  HTTPDefs,
  TLoggerUnit,
  TLevelUnit;

type

  { TfprLog }

  TfprLog = class(TLogger)
  public
    class procedure Log(const Msg: string; ARequest: TRequest = nil; Level: TLevel = nil);
    class procedure LogTrace(const Msg: string; Contents: string; ARequest: TRequest = nil);
  end;

implementation

{ TfprLog }

class procedure TfprLog.Log(const Msg: string; ARequest: TRequest; Level: TLevel);
begin
  if not Assigned(Level) then
    Level := TLevelUnit.Debug;
  GetInstance.Log(Level, Msg);
end;

class procedure TfprLog.LogTrace(const Msg: string; Contents: string; ARequest: TRequest);
var
  ContentStr: string;
begin
  ContentStr := Trim(Contents);
  if Length(ContentStr) > 100 then
    ContentStr := '['+Copy(ContentStr, 1, 100) + '] (concatenated)'
  else
    ContentStr := '['+Contents+ ']';
  Log(Msg + ' content: ' + ContentStr)
end;

end.

