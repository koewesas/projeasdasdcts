FROM fedorabaseimage

RUN microdnf -y install make binutils git glibc-devel rsync diffutils
RUN microdnf clean all

USER locuser
WORKDIR /home/locuser

RUN mkdir git
RUN mkdir bin
RUN mkdir buildfiles
RUN mkdir templates
ENV PATH="/home/locuser/bin:${PATH}"
WORKDIR /home/locuser/git

RUN mkdir /home/locuser/.ssh
COPY known_hosts /home/locuser/.ssh
COPY buildagent/buildagent /home/locuser
COPY buildagent/config/ppcx64_3.0.2 /home/locuser/bin
COPY buildagent/config/ppcx64_3.0.4 /home/locuser/bin
COPY buildagent/config/ppcx64_3.2.0 /home/locuser/bin
COPY buildagent/config/ppcx64_3.2.2 /home/locuser/bin
COPY buildagent/config/fppkg.cfg.template /home/locuser/templates
COPY buildagent/config/fppkg.cfg.304.template /home/locuser/templates

EXPOSE 8080

CMD [ "/home/locuser/buildagent", "-e" ]
